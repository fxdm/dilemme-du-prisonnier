**Code source R du tournoi**

Le tournoi tient en entier dans un seul gros script autonome. Ce fichier contient toutes fonctions nécessaires aux calculs des gains, les fonctions correspondant aux stratégies et le listing de toutes les stratégies concurrentes. Il suffit d'exécuter la totalité du script. Il reste juste à décommenter en fin de code les lignes qui executent le tournoi entier ou à seulement des petites parties.
