# Dilemme du prisonnier itéré en version interactive (R shiny)

**Auteur** : FX Dechaume-Moncharmont (fx.dechaume@univ-lyon1.fr)

## Description 

Ce dépôt rassemble les codes pour le [jeu interactif en R Shiny](https://fxdm.shinyapps.io/dilemme_app/) destiné à la découverte du "Dilemme du prisonnier itéré". L'utilisateur humain affronte un des huits robots suivant une des stratégies classiques. Ce site de dépôt permet de récupérer les codes R et de les faire tourner localement sur une machine équipée de R et R-studio sans se connecter au site shinyapps.

## Version en ligne 

[Version en ligne sur shinyapps](https://fxdm.shinyapps.io/dilemme_app/)

## Installation pour un usage local (avec R studio)

1. Téléchargez l'ensemble du répertoire en version zippé ("download source code", bouton avec une flèche, en haut à droite)
2. Décrompressez ce fichier zippé dans un répertoire local sur votre machine 
3. Ouvrez le script "run.R" dans RStudio 
4. Cliquez sur le bouton "Run App" (la flèche verte) en haut à droite de la fenêtre de script dans R studio

