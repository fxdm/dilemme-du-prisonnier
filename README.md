# Dilemme du prisonnier

**Auteur :** F.X. Dechaume-Moncharmont (université Lyon 1, fx.dechaume@univ-lyon1.fr)

Ensemble des codes utilisés pour le **cours d'initiation au dilemme du prisonnier itéré**. Ce dépôt comporte plusieurs parties distinctes : 
1. les codes R shiny pour le jeu interactif https://fxdm.shinyapps.io/dilemme_app/
2. les codes pour organiser le tournoi de stratégies avec les propositions des étudiants
